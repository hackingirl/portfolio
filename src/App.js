import './App.css';
import ProfileUser from './ProfileUser';
function App() {
  return (
    <div className="App">
      <div className="content">
        
        <ProfileUser />
      </div>  
    </div>
  );
}

export default App;

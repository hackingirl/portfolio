import { useState, useEffect } from "react";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import { faKey } from '@fortawesome/free-solid-svg-icons'
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMap } from '@fortawesome/free-solid-svg-icons'
import { FaSpinner } from 'react-icons/fa';

const iconHome = <FontAwesomeIcon icon={faHome} />
const iconEmail = <FontAwesomeIcon icon={faEnvelope} />
const iconPhone = <FontAwesomeIcon icon={faPhone} />
const iconCalendar = <FontAwesomeIcon icon={faCalendarAlt} />
const iconLocation = <FontAwesomeIcon icon={faMap} />
const iconPassword = <FontAwesomeIcon icon={faKey} />

const ProfileItem = () => {
  const url = 'https://randomuser.me/api/'
  const [item, setItem] = useState();
  const [valueItem, setValueItem] = useState();
  const items = [
    { name: 'name', icon: iconHome }, { name: 'email', icon: iconEmail },
    { name: 'date', icon: iconCalendar }, { name: 'address', icon: iconLocation },
    { name: 'phone', icon: iconPhone }, { name: 'password', icon: iconPassword },
  ];
  const [isLoading, setIsLoading] = useState(true)
  const [randomPerson, setRandomPerson] = useState(null)
  const menu = [];

  for (const it of items) {
    menu.push(<button key={it.name} onMouseEnter={(e) => handleMouseOver(it, e)} className="item" >{it.icon}</button>)

  }

  const fetchRandomPerson = async () => {
    setIsLoading(true)
    const response = await fetch(url)
    const data = await response.json()
    const person = data.results[0]
    const {
      phone,
      email,
      login: { password },
      name: { first, last },
      dob: { date },
      picture: { large: image },
      location: {
        street: { number, name },
      },
    } = person
    const newPerson = {
      image,
      phone,
      email,
      password,
      date,
      address: `${number} ${name}`,
      name: `${first} ${last}`,
    }
    setRandomPerson(newPerson);
    setIsLoading(false);
    setItem('My name is');
    setValueItem(newPerson.name);
  }
  useEffect(() => {
    fetchRandomPerson()
  }, [])
  let itemTitle = '';
  const handleMouseOver = (item, e) => {
    itemTitle = item.name;
    if (itemTitle === 'date') {
      let date = new Date(randomPerson[itemTitle]);
      randomPerson[itemTitle] = date.toLocaleDateString("en-US");
      itemTitle = 'birthday' 
    }
    setItem('My ' + itemTitle + ' is');
    setValueItem(randomPerson[item.name]);
  }

  return (
    <div className="profile-content">
      <div className="profile-img-div">
        {isLoading ? <FaSpinner icon="spinner" className="spinner" /> : <img src={randomPerson && randomPerson.image} alt="" />}
      </div>
      <div>
        <div className="subtitle-item">{item}</div>
        <div className="value-item">{valueItem}</div>
        <div className="navbar">
          {menu}
        </div>
      </div>
    </div>
  );
}

export default ProfileItem;
